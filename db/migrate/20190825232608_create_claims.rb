class CreateClaims < ActiveRecord::Migration[5.2]
  def change
    create_table :claims do |t|
      t.integer :user_id
      t.integer :coin_type
      t.decimal :amount, precision: 15, scale: 8

      t.timestamps
    end
  end
end
