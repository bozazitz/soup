class AddFieldsToClaims < ActiveRecord::Migration[5.2]
  def change
    add_column :claims, :claimed, :boolean, default: false
    add_column :claims, :key, :string
  end
end
