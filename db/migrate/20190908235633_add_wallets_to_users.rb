class AddWalletsToUsers < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :bitcoin_wallet, :string
    add_column :users, :litecoin_wallet, :string
    add_column :users, :ripple_wallet, :string
    add_column :users, :dash_wallet, :string
    add_column :users, :doge_wallet, :string
    add_column :users, :etherium_wallet, :string
  end
end
