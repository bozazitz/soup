class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable
  has_many :claims

  def can_claim?(coin_type)
    claims.send(coin_type).where(created_at: 5.minutes.ago..Time.now).present? ? false : true
  end

  def self.get_all_balances
    doge = ["https://block.io/api/v2/get_my_addresses/?api_key=d5b9-5af8-f022-64ff&page=",
            "https://block.io/api/v2/get_my_addresses/?api_key=5cf8-5822-d5c9-538b&page=",
            "https://block.io/api/v2/get_my_addresses/?api_key=e3c6-0724-a287-c7a1&page=",
            "https://block.io/api/v2/get_my_addresses/?api_key=32a6-0091-fe8b-5480&page=",
            "https://block.io/api/v2/get_my_addresses/?api_key=c8c9-1479-a71f-49b6&page=",
            "https://block.io/api/v2/get_my_addresses/?api_key=9d1b-1a46-78cd-11f2&page=",
            "https://block.io/api/v2/get_my_addresses/?api_key=ff9a-3cdd-5ce0-9abf&page="]

    grand_response = []
    doge_grand_total = 0.to_d

    doge.each do |doge_account|
      doge_bulk = 0.to_d
      response = JSON.parse(HTTParty.get(doge_account).body)
      response["data"]["addresses"].each do |address|
        doge_grand_total += address["available_balance"].to_f
        doge_bulk += address["available_balance"].to_f
      end
      grand_response << doge_bulk
    end

    grand_response << doge_grand_total
    grand_response
  end
end
