class Claim < ApplicationRecord
  belongs_to :user
  before_create :create_key

  enum coin_type: [:bitcoin, :litecoin, :ripple, :dash, :doge, :etherium]
  CLAIMS = {
    bitcoin: 0.00000001,
    litecoin: 0.00000010,
    ripple: 0.05,
    dash: 0.00000020,
    doge: 0.01,
    etherium: 0.00000005,
  }.freeze

  def self.get_amount_for(type)
    CLAIMS[type.to_sym]
  end

  def human_readable_amount
    sprintf("%.8f", amount)
  end

  def self.generate_short_link(key)
    links = [
      "http://btc.ms/api/?api=a50b0c5784ce9528f21a03f8e167e346f994a8a1",
      "http://oke.io/api/?api=e8b21f9e7f034a03f82c2da9939c3ba00f4cad32",
      "http://vivads.net/api?api=a58bdc690f644360b3c801cd1c1a284ac5a76536",
      "https://exe.io/api?api=50182fea0041a1fc9cbe9a483f51f5f9a5e0c609",
      "https://coin.mg/api?api=5ab1f5f650311f4ceb1a8fda73279636a36d60d0",
    ]
    JSON.parse(HTTParty.get(links.sample + "&url=https://faucetsoup.com/authorizations/" + key).body)["shortenedUrl"]
  end

  private

  def create_key
    o = [("a".."z"), ("A".."Z")].map(&:to_a).flatten
    self.key = (0...50).map { o[rand(o.length)] }.join
  end
end
