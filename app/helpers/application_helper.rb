module ApplicationHelper
  def human_readable amount
    sprintf('%.8f', amount)
  end

  def last_claim_formatter last_claim
    return "never" if last_claim.blank?

    time_ago_in_words(last_claim.created_at) + " ago"
  end
end
