class AnonymousController < ApplicationController
  skip_before_action :authenticate_user!
  layout 'anonymous'

  def index
    
  end
end
