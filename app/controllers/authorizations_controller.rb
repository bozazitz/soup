class AuthorizationsController < ApplicationController
  def show
    claim = Claim.find_by(key: params[:id], claimed: false)
    if claim.present?
      claim.update(claimed: true)
      redirect_to claims_path
    else
      redirect_to root_path
    end
  end
end
