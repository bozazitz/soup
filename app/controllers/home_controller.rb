class HomeController < ApplicationController
  def index
    @bitcoin = current_user.claims.bitcoin.sum(:amount)
    @litecoin = current_user.claims.litecoin.sum(:amount)
    @ripple = current_user.claims.ripple.sum(:amount)
    @dash = current_user.claims.dash.sum(:amount)
    @doge = current_user.claims.doge.sum(:amount)
    @etherium = current_user.claims.etherium.sum(:amount)
  end
end
