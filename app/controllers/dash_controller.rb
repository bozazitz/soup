class DashController < ApplicationController
  def index
    @balance = current_user.claims.dash.pluck(:amount).sum
    @last_claim = current_user.claims.dash.last
    @estimated_claim = Claim::CLAIMS[:dash]
    @claim = current_user.claims.new
  end
end
