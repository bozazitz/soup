class DogeController < ApplicationController
  def index
    @balance = current_user.claims.doge.pluck(:amount).sum
    @last_claim = current_user.claims.doge.last
    @estimated_claim = Claim::CLAIMS[:doge]
    @claim = current_user.claims.new
  end
end
