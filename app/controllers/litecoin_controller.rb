class LitecoinController < ApplicationController
  def index
    @balance = current_user.claims.bitcoin.pluck(:amount).sum
    @last_claim = current_user.claims.bitcoin.last
    @estimated_claim = Claim::CLAIMS[:bitcoin]
    @claim = current_user.claims.new
  end
end
