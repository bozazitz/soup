class ClaimsController < ApplicationController
  def index
    @claims = current_user.claims.order(created_at: :desc)
  end

  def create
    claim = current_user.claims.new(
      coin_type: claim_params[:coin_type],
      amount: Claim.get_amount_for(claim_params[:coin_type]),
    )
    if claim.valid? && current_user.can_claim?(claim_params[:coin_type])
      claim.save
      redirect_to Claim.generate_short_link(claim.key)
    else
      redirect_to root_path, notice: "you can only claim once every 5 minutes!"
    end
  end

  def new
    @claim = current_user.claims.new
  end

  def show
    @claim = current_user.claims.find params[:id]
  end

  private

  def claim_params
    params.require(:claim).permit(:coin_type)
  end
end
