require 'test_helper'

class AnonymousControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get anonymous_index_url
    assert_response :success
  end

end
