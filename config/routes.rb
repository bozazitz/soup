Rails.application.routes.draw do
  get "anonymous/index"
  devise_for :users
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  resources :bitcoin, only: :index
  resources :litecoin, only: :index
  resources :ripple, only: :index
  resources :dash, only: :index
  resources :doge, only: :index
  resources :etherium, only: :index
  resources :claims, only: [:index, :new, :create, :show]
  resources :monitor, only: :index
  resources :authorizations, only: :show
  # resources :wallets, only: [:index, :update]

  unauthenticated :user do
    root to: "anonymous#index"
  end

  authenticated :user do
    root to: "home#index"
  end
end
